package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	const length = 60
	var previous, same int
	allTheSame := true

	var queue [length]int
	for i := range queue[:] {
		queue[i] = int(rand.Float32()*6 + 1)
		fmt.Print(queue[i])
	}
	for i := 0; i < 6; i++ {
		fmt.Println()
		index := int(i)
		fmt.Print(index + 1)
		for index < length {
			previous = index
			index = index + queue[index]
		}
		if i == 0 {
			same = previous
		} else if same != previous {
			allTheSame = false
		}
		fmt.Print(" -> ")
		fmt.Print(previous)
	}
	fmt.Println()
	fmt.Println(allTheSame)
}
